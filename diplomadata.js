var diplomadata = {
  documents: [],
  push: function(doc) {
    this.documents.push(doc);
  },
};
diplomadata.push(  {
  autors: `Васильев  Ф.П.`,
  title: `методы оптимизации`,
  pot: `оптимизация функция минимизация дифференциальные уравнения численные методы`,
  procesed:null,
});
diplomadata.push(  {
  autors: `Корнейчук Н.П., Лигун А. А., Доронин В.Г.`,
  title: `аппроксимация с ограничениями`,
  pot: `аппроксимация, неравенство, приближения, сплайн`,
  procesed:null,
});
diplomadata.push(  {
  autors: `Лоран П.-Ж.`,
  title: `Аппроксимация и оптимизация`,
  pot: `сплайн, аппроксимация, интерполяция, экстраполяция, оптимизация`,
  procesed:null,
});
diplomadata.push(  {
  autors: `Самарский А.А., Гулин А.В.`,
  title: `Численные методы математической физики`,
  pot: `аппроксимация, разности,дифференциальные уравнения`,
  procesed:null,
});
diplomadata.push(  {
  autors: `Лебедев П. Д. , Ушаков А. В.`,
  title: `Аппроксимация множеств на плоскости оптимальными наборами кругов`,
  pot: `сеть, круг, аппроксимация, кривая, многоугольник`,
  procesed:null,
});

diplomadata.push(  {
  autors: `Бляшке В.`,
  title: `Круг и шар`,
  pot: `круг, шар, минимизация, симметрия`,
  procesed:null,
});
diplomadata.push(  {
  autors: `Леонтьев В.`,
  title: `Экономические эссе`,
  pot: `круг, интерес, экономика, политика`,
  procesed:null,
});
diplomadata.push(  {
  autors: `Смит Р. С, Эренберг Р. Дж.`,
  title: `Современная экономика труда`,
  pot: `труд, политика, экономика`,
  procesed:null,
});

if (typeof(exports) !== "undefined") {
  if (typeof(module) !== "undefined" && module.exports) {
    exports = module.exports = diplomadata;
  }
  exports.diplomadata = diplomadata;
}
