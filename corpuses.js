const morphy = require('./morphy');
var Corpus = require("./corpus");
module.exports = function createCorpuses(data) {
  var corpuses = [];
  data.map((corpusItem) => {
    if (corpusItem.procesed) {
      let index = corpuses.push(new Corpus())

    corpusItem.procesed.map((item,indexWord) => {
      let typeWord = morphy.ru.getPartOfSpeech(item);
      if (typeWord && (!typeWord.find((qitem) => {
          return (qitem == 'СОЮЗ') || (qitem == 'ПРЕДЛ') || (qitem == 'МЕЖД') || (qitem == 'ЧАСТ') || (qitem == 'МС') || (qitem == 'МС-П');
        }))) {
        corpuses[index-1].pushWord = (morphy.ru.lemmatize(item)[0]);
      }
    })
    corpuses[index-1].title=corpusItem.title;
    corpuses[index-1].autors=corpusItem.autors;
corpuses[index-1].calcIndex();
  }
  }) ;

  return corpuses;
}
