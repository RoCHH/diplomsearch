﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TextRequest
{
    public partial class Form1 : Form
    {
        List<String> S_mass = new List<string>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button9_Click(object sender, EventArgs e)////files open 
        {
            if(openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                String[] paths = openFileDialog1.FileNames;
                foreach(String str in paths)
                {
                    listBox2.Items.Add(str);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)//Добавить выделенный
        {
            if (listBox2.SelectedItem != null)
            {
                String path = listBox2.SelectedItem.ToString();
                StreamReader streamReader = new StreamReader(path, Encoding.Default);
                String text = "";
                while (!streamReader.EndOfStream)
                {
                    text += streamReader.ReadLine() + "\r\n";
                }
           
                if (!text.Equals(String.Empty))
                {
                    IStemmer stemmer = new RussianStemmer();
                    String all_text = text;
                    char[] separetor = { ' ', '.', '"' };
                    char[] signs = { '.', ',', '!', '?', '/', '"', ':', '№', '-', ';', '·', '«', '»' };
                    foreach (char s in signs)
                    {
                        all_text = all_text.Replace(s, ' ');
                    }
                    string[] tokenize_mass = all_text.Split(separetor, StringSplitOptions.RemoveEmptyEntries);
                    string tokenize_text = "";

                    foreach (string str in tokenize_mass)
                    {
                        double temp_num;
                        if (!Double.TryParse(str, out temp_num) && str.Length > 2)
                        {
                            tokenize_text += stemmer.Stem(str)+" ";
                        }
                    }
                    String[] row_txt = { path, tokenize_text };
                    dataGridView1.Rows.Add(row_txt);
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)//Добавить в запрос
        {
            if(!txtRequest.Text.Equals(String.Empty))
            {
                listBox1.Items.Add(txtRequest.Text);
                IStemmer stemmer = new RussianStemmer();
                String all_text = txtRequest.Text;
                char[] separetor = { ' ', '.', '"' };
                char[] signs = { '.', ',', '!', '?', '/', '"', ':', '№', '-', ';', '·', '«', '»' };
                foreach (char s in signs)
                {
                    all_text = all_text.Replace(s, ' ');
                }
                string[] tokenize_mass = all_text.Split(separetor, StringSplitOptions.RemoveEmptyEntries);
                string tokenize_text = "";
                if(tokenize_mass.Length > 1)
                { 
                    foreach (string str in tokenize_mass)
                    {
                        double temp_num;
                        if (!Double.TryParse(str, out temp_num) && str.Length > 2)
                        {
                            tokenize_text += stemmer.Stem(str) + " ";
                        }
                    }
                    S_mass.Add(stemmer.Stem(tokenize_text));
                }
                else {S_mass.Add(stemmer.Stem(txtRequest.Text)); }
                
                txtRequest.Text = "";
            }
        }

        private void button13_Click(object sender, EventArgs e)//Расчитать
        {
            double entropy_S = 0;
            if (listBox1.Items.Count > 0 && dataGridView1.Rows != null)
            {
                List<double> rating = new List<double>();
                List<double> w_rating = new List<double>();
                entropy_S = Math.Log(listBox1.Items.Count, 2);
                label4.Text = "Энтропия запроса: " + entropy_S;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    String key_words = row.Cells[1].Value.ToString();
                    string[] separator = { " ", "\n\r", "\r\n" };
                    List<String> list_key = key_words.Split(separator, StringSplitOptions.RemoveEmptyEntries).ToList<String>();
                  
                    double count = list_key.Count;
                    double result = 0;
                    double sumgain = 0;
                    foreach (String uniq in list_key.Distinct<String>())
                    {
                        double element_count = list_key.Count<String>(x => x.Equals(uniq));
                        double p = (element_count / count);
                        result += (p * Math.Log(p, 2));
                    }
                    foreach (String str in S_mass)
                    {
                        double element_count = list_key.Count<String>(x => x.Contains(str));
                        double p = 0, non_p = 1, uniq_entropy = 0;
                        if (element_count != 0)
                        {
                            p = (element_count / count);
                            non_p = ((count - element_count) / count);
                            uniq_entropy = (-1) * (p * Math.Log(p, 2) + non_p * Math.Log(non_p, 2));
                            sumgain += element_count * uniq_entropy;
                        }
                    }
                    row.Cells[2].Value = (result * (-1)).ToString();
                    row.Cells[3].Value = (result * (-1) - (1 / count) * (sumgain));
                    row.Cells[4].Value = ((result * (-1) - (1 / count) * (sumgain)) / (result * (-1)));
                    rating.Add(((result * (-1) - (1 / count) * (sumgain)) / (result * (-1))));
                    row.Cells[6].Value = ((1 / count) * (sumgain)) / (result * (-1));

                    list_key.Concat(S_mass);
                    double w_sumgain = 0;
                    foreach (String w_str in list_key.Distinct<String>())
                    {
                        double element_count = list_key.Count<String>(x => x.Equals(w_str));
                        double p = 0, non_p = 1, uniq_entropy = 0, w = 0;
                        if (S_mass.Contains(w_str))
                        { w = 1;}
                        else { w = ((1 / count) * (sumgain)) / (result * (-1)); }
                        p = (element_count / count);
                        non_p = ((count - element_count) / count);
                        uniq_entropy = (-1) * (p * Math.Log(p, 2) + non_p * Math.Log(non_p, 2));
                        w_sumgain += element_count * uniq_entropy * w;
                    }
                    row.Cells[7].Value = ((result * (-1) - (1 / count) * (w_sumgain)) / (result * (-1)));
                    w_rating.Add(((result * (-1) - (1 / count) * (w_sumgain)) / (result * (-1))));
                }
                rating.Sort((x, y) => x.CompareTo(y));
                w_rating.Sort((x, y) => x.CompareTo(y));
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    row.Cells[5].Value = rating.IndexOf(Convert.ToDouble(row.Cells[4].Value)) + 1;
                    row.Cells[8].Value = w_rating.IndexOf(Convert.ToDouble(row.Cells[7].Value));
                }

            }
        }

        private void button11_Click(object sender, EventArgs e)//Очистить запрос
        {
            listBox2.Items.Clear();
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem != null)
            {
                listBox2.Items.Remove(listBox2.SelectedItem);
            }
        }

        private void посмотретьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem != null)
            {
                String path = listBox2.SelectedItem.ToString();
                StreamReader streamReader = new StreamReader(path, Encoding.Default);
                String text = "";
                while (!streamReader.EndOfStream)
                {
                    text += streamReader.ReadLine() + "\r\n";
                }
                TextView form2 = new TextView(text, path);
                form2.Show();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)//Добавить все
        {
            if (listBox2.Items != null)
            {
                foreach (String path in listBox2.Items)
                {
                    StreamReader streamReader = new StreamReader(path, Encoding.Default);
                    String text = "";
                    while (!streamReader.EndOfStream)
                    {
                        text += streamReader.ReadLine() + "\r\n";
                    }

                    if (!text.Equals(String.Empty))
                    {
                        IStemmer stemmer = new RussianStemmer();
                        String all_text = text;
                        char[] separetor = { ' ', '.', '"' };
                        char[] signs = { '.', ',', '!', '?', '/', '"', ':', '№', '-', ';', '·', '«', '»' };
                        foreach (char s in signs)
                        {
                            all_text = all_text.Replace(s, ' ');
                        }
                        string[] tokenize_mass = all_text.Split(separetor, StringSplitOptions.RemoveEmptyEntries);
                        string tokenize_text = "";

                        foreach (string str in tokenize_mass)
                        {
                            double temp_num;
                            if (!Double.TryParse(str, out temp_num) && str.Length > 2)
                            {
                                tokenize_text += stemmer.Stem(str) + " ";
                            }
                        }
                        String[] row_txt = { path, tokenize_text };
                        dataGridView1.Rows.Add(row_txt);
                    }
                }
            }
        }

        private void удалитьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedItem != null)
            {
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
        }
    }
}
