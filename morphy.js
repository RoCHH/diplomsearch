const Morphy = require('phpmorphy').default;

const morphy = {
  ru: new Morphy('ru', {
    //  nojo:                false,
    storage: Morphy.STORAGE_MEM,
    predict_by_suffix: true,
    predict_by_db: true,
    graminfo_as_text: true,
    use_ancodes_cache: true,
    resolve_ancodes: Morphy.RESOLVE_ANCODES_AS_TEXT
  }),
  ua: new Morphy('ua', {
    //  nojo:                false,
    storage: Morphy.STORAGE_MEM,
    predict_by_suffix: true,
    predict_by_db: true,
    graminfo_as_text: true,
    use_ancodes_cache: false,
    resolve_ancodes: Morphy.RESOLVE_ANCODES_AS_TEXT
  }),
  en: new Morphy('en', {
    //  nojo:                false,
    storage: Morphy.STORAGE_MEM,
    predict_by_suffix: true,
    predict_by_db: true,
    graminfo_as_text: true,
    use_ancodes_cache: false,
    resolve_ancodes: Morphy.RESOLVE_ANCODES_AS_TEXT
  })
};

if (typeof(exports) !== "undefined") {
  if (typeof(module) !== "undefined" && module.exports) {
    exports = module.exports = morphy;
  }
  exports.morphy = morphy;
}
