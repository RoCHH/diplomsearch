const morphy = require('./morphy');
var Corpus = require("./corpus");
module.exports = function createCorpuses(corpusItem) {
  var corpus = new Corpus();


    corpusItem.map((item) => {
      let typeWord = morphy.ru.getPartOfSpeech(item);
      if (typeWord && (!typeWord.find((qitem) => {
          return (qitem == 'СОЮЗ') || (qitem == 'ПРЕДЛ') || (qitem == 'МЕЖД') || (qitem == 'ЧАСТ') || (qitem == 'МС') || (qitem == 'МС-П');
        }))) {
        corpus.pushWord = (morphy.ru.lemmatize(item)[0]);
      }
    })
corpus.calcIndex();
  return corpus;
}
