import Vue from 'vue'
import addApp from './addApp.vue'

import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
new Vue({
  el: '#addApp',
  render: h => h(addApp)
})
