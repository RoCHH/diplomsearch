const replaceall = require("replaceall");

var TextCleaner = {
  signs: `.,!? /|":'-№;·«»()_*–&^%$#\n\t\r1234567890[]{}…—=+'`.split(''),
  CleanData: function(data) {
    return data.map((doc) => {

      let pot = doc.title+" "+doc.pot;
    this.signs.map((ss) => {
        pot = replaceall(ss, ' ', pot);
      });
      pot = pot.trim();

      doc.procesed = pot.split(' ').filter((item) => {return !!item.length});
      doc.procesed =doc.procesed.map((w) => {return replaceall(' ','',w)})

      return doc;
    })
  },
  cleanQuery:function(query) {
      let pot = query;
      let res;
    this.signs.map((ss) => {
        pot = replaceall(ss, ' ', pot);
      });
      pot = pot.trim();

      res = pot.split(' ').filter((item) => {return !!item.length});
      res=res.map((w) => {return replaceall(' ','',w)})

      return res;

  }

}

if (typeof(exports) !== "undefined") {
  if (typeof(module) !== "undefined" && module.exports) {
    exports = module.exports = TextCleaner;
  }
  exports.TextCleaner = TextCleaner;
}
