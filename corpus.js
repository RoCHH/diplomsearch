class Word {
  constructor(inWord) {
    this.word = inWord;
    this._count = 1;
    this.index = -1;
    this.data = null;

  }
  set count(qwe) {
    this._count++
  }
  get count() {
    return this._count
  }
  toString() {
    return this.word
  }
}

module.exports = class Corpus {
  constructor() {
    this._keys = new Map();
    this.data = null;
    this.title=null;
    this.autors=null;
  }
  calcIndex() {
    let index = 1;
    this._keys.forEach((item) => {

      item.index = index;
      index++;
    })
  }
  get length() {
    return this._keys.size;
  }
  get countWords() {
    let res = 0;
    this._keys.forEach((item) => {
      res += item.count;
    })
    return res;
  }
  get keys() {}
  set pushWord(word) {
    if (this._keys.has(word)) {
      this._keys.get(word).count++;
    } else {
      this._keys.set(word, new Word(word))
    }
  }
}
