## Build Setup

# install dependencies
npm install


# build for production with minification
npm run build

# server at localhost:3000
node main