var express = require('express');
var bodyParser = require("body-parser");
var clone = require('clone');
const fs = require('fs');
const TextCleaner = require('./textcleaner');
// var data = require("./diplomadata");
var Corpus = require("./corpus");
var Corpuses = require("./corpuses")
var Query = require("./query")
function Log2(x) {
  return Math.log(x) / Math.log(2);
}

var data;
if (fs.existsSync("file.bin")) {
  data =JSON.parse(fs.readFileSync("file.bin",'utf8'));
}else{
  data= require("./diplomadata").documents;
  fs.writeFile("file.bin", JSON.stringify(data), 'utf8', (err)=>{
     if(err) console.log(err)
     else console.log('File "file.bin" saved')
  })
}

var corpusesR = Corpuses(TextCleaner.CleanData(data));
var app = express();
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(express.static('public'));
app.post('/add', function(req, res) {
  console.log(req.body);
  data.push(req.body);
  // fs.writeFile("file.bin", JSON.stringify(data.documents), 'utf8', (err)=>{
  //    if(err) console.log(err)
  //    else console.log('File saved')
  // })
  corpusesR = Corpuses(TextCleaner.CleanData(data));
  res.send(`<script>alert("Додано");location.href='/';</script>`);
});
app.post('/', function(req, res) {
  // console.log(JSON.stringify(req.body));

var corpuses  = clone(corpusesR);


  // data = TextCleaner.CleanData(data.documents);
  // var txtQuery = `аппроксимация  круговыми сплайнами`;
  var txtQuery = req.body.q;
  // console.log(TextCleaner.cleanQuery(txtQuery));
  var query = Query(TextCleaner.cleanQuery(txtQuery));

  var entropy_S = Log2(query.length);
  var rating = [];
  var w_rating = [];


  console.log('entropy query = ' + entropy_S);

  corpuses.map((document, index) => {
    let list_key = document._keys;
    let count = document.countWords;
    let result = .0;
    let sumgain = .0;
    for (let [key, value] of list_key) {
      let element_count = value._count;
      let p = element_count / count;
      result += p * Log2(p);
    }

    for (let [key, value] of query._keys) {
      let element_count = value._count;
      let p = element_count / count,
        non_p = 1,
        uniq_entropy = .0;
      non_p = ((count - element_count) / count);
      uniq_entropy = (-1) * (p * Log2(p) + non_p * Log2(non_p));
      sumgain += element_count * uniq_entropy;
    }

    document.data = {
      entropy: (result * (-1)),
      prirost: (result * (-1) - (1 / count)* (sumgain)),
      otnositelniy: ((result * (-1) - (1 / count) * (sumgain)) / (result * (-1))),
      obsheeVesovoe: ((1 / count) * (sumgain)) / (result * (-1))
    };
        rating.push(((result * (-1) - (1 / count) * (sumgain)) / (result * (-1))));

    var newDoc = new Corpus;
    newDoc._keys = new Map(document._keys);
    for (let [key, value] of query._keys) {
      newDoc.pushWord = value.word;
    }
    newDoc.calcIndex();
     count = newDoc.countWords;
    let w_sumgain = .0;
    for (let [key, value] of newDoc._keys) {
      let element_count = value._count;
      let p = .0,
        non_p = 1.0,
        uniq_entropy = .0,
        w = .0;
      if (query._keys.has(key)) {
        w = 1.0;
      } else {
        {
          w = ((1 / count) * (sumgain)) / (result * (-1));
        }
      }
      p = (element_count / count);
      non_p = ((count - element_count) / count);
      uniq_entropy = (-1) * (p * Log2(p) + non_p * Log2(non_p));
      w_sumgain += element_count * uniq_entropy * w;
    }
    document.data.otnositelniy2 = ((result * (-1) - (1 / count) * (w_sumgain)) / (result * (-1)));
    w_rating.push(((result * (-1) - (1 / count) * (w_sumgain)) / (result * (-1))));
    //  console.log(document.data);
  })
  let qrating = rating.sort((a, b) => {
    return a - b;
  })
  let qw_rating = w_rating.sort((a, b) => {
    return a - b;
  })
  //console.log(qrating);
  // console.log(qw_rating);
  corpuses.map((document, index) => {
    document.data.ratingS = qw_rating.indexOf(document.data.otnositelniy2) + 1;
    document.data.rating = qrating.indexOf(document.data.otnositelniy) + 1;
  });
  res.send(corpuses);
  corpuses.map((document, index) => {
    // console.log(document);
  })
  console.log(corpusesR);

});

app.listen(3000, function() {
  console.log('Example app listening on port 3000!');
});





// for (variable of corpuses) {
//   // console.log(variable.length);
//   // console.log(variable.countWords);
//
//   // for (let [key, value] of variable._keys) {
//   //  console.log(value);
//   // }
// }

// for (let [key, value] of query._keys) {
//  console.log(value);
// }



// //починить контеинс из шарпового кода ! я просто упустил некотрые тут !




// var count = res.length;
// var result = .0;
// var sumgain = .0;
// //примечание  : добавить сюда формы слова не порезанные на уникальность  ибо лемитайзер выдает ток уникальные в arrey mode
// res.map((item) => {
//   //количество всречаний слова в тексте
//   var element_count = 1; ///лемитайзер выдает все уникальные пофиксить //list_key.Count<String>(x => x.Equals(uniq));
//   var p = (element_count / count); //делим количество всречаимости слова в тексте на общее колво слов
//   result += (p * getBaseLog(2, p));
//
// });
// //////////////////////////////
// txtRequest.map((str) => {
//   var element_count = res.filter(x => x.includes(str)).length;
//   var p = 0,
//     non_p = 1,
//     uniq_entropy = 0;
//   if (element_count != 0) {
//     p = (element_count / count);
//     non_p = ((count - element_count) / count);
//     uniq_entropy = (-1) * (p * getBaseLog(2, p) + non_p * getBaseLog(2, non_p));
//     sumgain += element_count * uniq_entropy;
//   }
// });
// var exit = [];
// exit.push(result * (-1));
// exit.push(result * (-1) - (1 / count) * (sumgain));
// exit.push((result * (-1) - (1 / count) * (sumgain)) / (result * (-1)));
// rating.push(((result * (-1) - (1 / count) * (sumgain)) / (result * (-1))));
// exit.push(((1 / count) * (sumgain)) / (result * (-1)));
//
// // list_key.Concat(S_mass);
// var w_sumgain = 0;
//foreach (String w_str in list_key.Distinct<String>())
//{
//double element_count = list_key.Count<String>(x => x.Equals(w_str));
//double p = 0, non_p = 1, uniq_entropy = 0, w = 0;
//if (S_mass.Contains(w_str))
//{ w = 1;}
//else { w = ((1 / count) * (sumgain)) / (result * (-1)); }
//p = (element_count / count);
//non_p = ((count - element_count) / count);
//uniq_entropy = (-1) * (p * Math.Log(p, 2) + non_p * Math.Log(non_p, 2));
//w_sumgain += element_count * uniq_entropy * w;
//}
//row.Cells[7].Value = ((result * (-1) - (1 / count) * (w_sumgain)) / (result * (-1)));
//w_rating.Add(((result * (-1) - (1 / count) * (w_sumgain)) / (result * (-1))));
//}
//rating.Sort((x, y) => x.CompareTo(y));
//w_rating.Sort((x, y) => x.CompareTo(y));
//foreach (DataGridViewRow row in dataGridView1.Rows)
//{
//row.Cells[5].Value = rating.IndexOf(Convert.ToDouble(row.Cells[4].Value)) + 1;
//row.Cells[8].Value = w_rating.IndexOf(Convert.ToDouble(row.Cells[7].Value));
//}

//
//
//
// console.log(morphy.ru.lemmatize(qdata[0]));
// console.log(morphy.ua.lemmatize('котяче'));



// console.log(exit);
